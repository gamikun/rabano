# Rabano

Daemon management multiplatform.

SO's
* OS X. Working
* Linux. Pending
* Windows. Pending

## Python

The recommended python is 2.7.6

* wxPython 3.0.0
* numpy

## Implementation summary

### Already implemented

* Apache
* NGINX
* Memcached
* MySQL server

### Not implemented yet

* PostgreSQL
* MongoDB