from setuptools import setup
from sys import argv

len_arg = len(argv)

if len_arg > 1:

  if argv[1] == 'py2app':
    APP = ['src/main.py']
    DATA_FILES = [
      ('images',[
        'src/images/nginx.png', 'src/images/apache.png', 'src/images/mysql.png',
        'src/images/php.png', 'src/images/status-gray.png', 'src/images/postgre-sql.png',
        'src/images/python.png', 'src/images/status-green.png', 'src/images/uwsgi.png',
        'src/images/tb-icon.png',
      ]),
    ]
    OPTIONS = {
      'argv_emulation': True,
      'iconfile': 'src/images/icon.icns',
    }

    setup(
      name="Rabano",
      app=APP,
      data_files=DATA_FILES,
      options={'py2app': OPTIONS},
      setup_requires=['py2app'],
    )

  else:
    print "Command not found"

else:
  print "Command not received"