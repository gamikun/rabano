import sqlite3, os
from functions import get_config_dir

data_dir = get_config_dir()

def getdb():
  return sqlite3.connect(data_dir+'/data.db')

def save_instance(name, binary="", params=[], type=None):
  assert not type is None
  with getdb() as db:
    cursor = db.cursor()
    cursor.execute("""
      INSERT INTO instance(name, binary, params)
      VALUES(:name, :binary, :params)
    """, {'name' : name, 'binary' : binary, 'params' : ",".join(params)})
    db.commit()
    return cursor.lastrowid

def get_all_instances():
  with getdb() as db:
    c = db.cursor()
    return c.execute("SELECT * FROM instance").fetchall()