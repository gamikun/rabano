from rabano.system import Service, Elevation
import subprocess as sproc, os


class NginxService(Service):
  def __init__(self, binary=None, config_file=None, pid_file=None):
    super(NginxService, self).__init__(binary,
      config_file=config_file,
      pid_file=pid_file
    )
    self.type_name = "nginx"

  def start(self, callack=None):
    assert retcode == 0, (retcode, stdout, stderr,)
    return True

class PostgresService(Service):

  def __init__(self, binary=None, data_dir=None):
    super(PostgresService, self).__init__(binary, instance_dir=data_dir)
    assert not data_dir is None
    self.type_name = "postgresql"

  def start(self, callback=None, password=''):
    print self
    return
    params=[self.binary,
      '-D', self.instance_dir,
      'start',
    ]

    pp = Elevation.popen_su(params, user='postgres')
    pp.communicate()
    retcode = pp.returncode
    if retcode == 0:
      pp = Sudo.popen(['chmod','o+x', self.config_file], with_stdin=True)
      pp.communicate(password + '\n')

    return retcode

  def stop(self):
    params = [self.binary,
      '-D', self.instance_dir,
      'stop', '-s', '-m', 'fast',
    ]
    p = sproc.Popen(params, stdout=sproc.PIPE)
    p.communicate()
    return True if p.returncode == 0 else False

  """
  def is_running(self):
    params = ['ps','aux']
    run_command = "%s -D %s" % (self.binary, self.instance_dir,)
    p = sproc.Popen(params, stdout=sproc.PIPE)
    stdout, stderr = p.communicate()
    if p.returncode == 0:
      lines = stdout.split('\n')
      for l in lines:
        if run_command in l:
          return True
      return False
    else:
      return False
  """


class PhpService(Service):
  def start(self):
    params=[self.binary,
      '-c', self.config_file,
      '--pid', self.pid_file,
      '-D'
    ]

class MemcachedService(Service):

  def __init__(self, binary=None, pid_file=None):
    super(MemcachedService, self).__init__(binary,
      pid_file=pid_file, need_sudo=False
    )

  def start(self):
    params = [self.binary, '-d', '-P', self.pid_file]
    p = sproc.Popen(params, stdout=sproc.PIPE)
    stdout, stderr = p.communicate()
    return 0 if p.returncode == 0 else 1

  def stop(self):
    super(MemcachedService, self).stop()
    try:
      os.remove(self.pid_file)
    except: pass;

class MysqlService(Service):

  def start(self):
    params = [self.binary,
      '--pid-file', self.pid_file
    ]

    s = sproc.Popen(params)
    p.communicate()

    return p.returncode


class ApacheService(Service):

  def start(self, password=''):
    p = Elevation.popen(command=self.get_params('start'), with_stdin=True)
    out, err = p.communicate(password + '\n')
    return p.returncode

  def stop(self, password=''):
    p = Elevation.popen(command=self.get_params('stop'), with_stdin=True)
    out, err = p.communicate(password + '\n')
    return p.returncode

  def get_params(self, command='start'):
    return [self.binary,
      '-c', "PidFile %s" % self.pid_file,
      '-k', command]



