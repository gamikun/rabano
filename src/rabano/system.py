import subprocess as sproc


class Service(object):
  """ This class provides a standard
      service killer and starter """

  STOPPED = 0
  RUNNING = 1
  STARTING = 2
  FAILED = 3
  STOPPING = 4

  def __init__(self, binary=None, params=None, pid_file=None, type=None,
               instance_dir=None, config_file=None, need_sudo=True):
    self.status = Service.STOPPED
    self.binary = binary
    self.params = params
    self.pid_file = pid_file
    self.instance_dir = instance_dir
    self.config_file = config_file
    self.type = type
    self.cached_pid = None
    self.is_starting = False
    self.is_stopping = False
    self.need_sudo = need_sudo

  def is_running(self):
    """ Checks if the process is running """
    pid = self.get_pid()
    if pid > 0:
      p = sproc.Popen(['ps', '-p', str(pid)], stdout=sproc.PIPE)
      p.communicate()
      return True if p.returncode == 0 else False
    else:
      return False

  def get_pid(self):
    if not self.cached_pid is None:
      return self.cached_pid
    else:
      try:
        with open(self.pid_file, 'r') as fp:
          self.cached_pid = int(fp.read().strip())
          return self.cached_pid
      except:
        return 0

  def start(self):
    assert self.status == Service.STOPPED

    self.status = Service.STARTING

    if self.type == 'nginx':

      print (retcode, stdout, stderr)

    elif self.type == 'postgresql':
      retcode, stdout, stderr = Elevation.popen_su([self.binary] + self.params,
        user='postgres'
      )

    if retcode == 0:
      self.status = Service.RUNNING
    else:
      self.status = Service.STOPPED

  def stop(self, password=''):

    self.status = Service.STOPPING

    if not self.need_sudo:
      pp = sproc.Popen(['kill', '-QUIT', str(self.get_pid())])
      pp.communicate()

    else:
      pp = Elevation.popen(command=['kill', '-QUIT', str(self.get_pid())],
        with_stdin=True)
      pp.communicate(password + '\n')

    if pp.returncode == 0:
      self.status = Service.RUNNING
    else:
      self.status = Service.STOPPED

    self.cached_pid = None

    return pp.returncode


class Elevation(object):
  """ Everything that needs to be executed
      in sudo mode in whole application is
      supposed to run with this classs     """

  cached_password = None
  
  @staticmethod
  def run_privileged(params):
    pass

  @staticmethod
  def kill_process(process_id):
    return Sudo.popen(['kill', str(process_id)])

  @staticmethod
  def popen(params=None, command=None, callback=None, with_stdin=False):
    """
    if not command is None:
      the_command = command
    else:
      prm = ['' for i in range(len(params))]
      for i, item in enumerate(params):
        if ' ' in item or item.startswith('"'):
          prm[i] = '"' + item.replace('"', r'\"') + '"'
        else:
          prm[i] = item
      the_command = ' '.join(prm)
    """
    
    p = sproc.Popen(['sudo', '-S'] + command if not command is None else [],
      stdout=sproc.PIPE,
      stderr=sproc.PIPE,
      stdin=sproc.PIPE
    )

    return p

  @staticmethod
  def popen_su(params, user=None):
    assert not user is None
    prm = ['' for i in range(len(params))]
    for i, item in enumerate(params):
      if ' ' in item or item.startswith('"'):
        prm[i] = '"' + item.replace('"', r'\"').replace("'", "\'") + '"'
      else:
        prm[i] = item

    """ TODO: fix paths with spaces """

    #prm = 
    p = Elevation.popen(command=['su',user,'-c', ' '.join(prm)])

    return p

def find_process_pipe(user=None, text=None):
  """ Look for a process running and returns its pipe"""
  assert not user is None or not rutine is None
  str_search = 'ps aux'

  if not user is None:
    str_search += ' | grep "^%s"' % user.replace('"', r'\"')

  if not text is None:
    str_search += ' | grep "%s"' % text.replace('"', r'\"')

  p = sproc.Popen(str_search, shell=True, stdout=sproc.PIPE)

  return p

def find_process_output(user=None, text=None):
  """ Look for a process running and returns its output"""
  p = find_process_pipe(user=user, text=text)
  stdout, stderr = p.communicate()
  return stdout if p.returncode == 0 else None

def find_process_id(user=None, text=None):
  """ Check for a process running and returns its ID """
  stdout = find_process_output(user=user, text=text)
  if not stdout is None:
    proc_split = stdout.split()
    return int(proc_split[1])
  else:
    return 0

