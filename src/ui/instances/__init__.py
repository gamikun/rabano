# -*- coding: utf-8 -*-
import data, shutil, os, getpass, wx
from functions import \
  get_config_dir, \
  get_default_config_dir, \
  get_instance_dir, \
  create_instance


class NginxFrame(wx.Frame):
  def __init__(self, parent=None):
    super(NginxFrame, self).__init__(parent, title="New NGINX instance", 
      style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)
    self.panel = wx.Panel(self)
    self.parent = parent

    # Title
    self.label_title = wx.StaticText(
      self.panel, wx.ID_ANY, "Title:", pos=(30, 20))
    self.title_edit = wx.TextCtrl(
      self.panel, wx.ID_ANY, pos=(80,20), size=(200, 24))

    # Binary
    self.binary_label = wx.StaticText(
      self.panel, wx.ID_ANY, "Binary:", pos=(30, 65))
    self.binary_edit = wx.TextCtrl(
      self.panel, wx.ID_ANY, pos=(80,65), size=(200,24))
    self.binary_edit.SetValue('/opt/local/sbin/nginx')

    # Save and cancel button
    self.button_save = wx.Button(
      self.panel, wx.ID_ANY, label="Save", size=(80,30), pos=(300, 180))

    # Events
    self.button_save.Bind(wx.EVT_LEFT_UP, self.save_it)

  def save_it(self, event):
    title = self.title_edit.GetValue()
    binary = self.binary_edit.GetValue()
    if title != '' and binary != '':
      instance_id = create_instance(title, binary, type='nginx')
      default_dir = get_default_config_dir('nginx')

      # 2. Add it to list in main frame
      self.parent.list.AddItem("Hola", "Adio",
        icon=wx.Icon("nginx.png", wx.BITMAP_TYPE_PNG))

      # 4. Tomar la información de nginx.conf y cambiar
      #    el directorio del log de errores.
      config_dump = ''
      with open(default_dir + '/nginx.conf', 'r') as f:
        for l in f:
          if l.startswith('error_log'):
            config_dump += 'error_log %s/error_log.txt;\n' % instance_dir
          elif l.startswith('user'):
            config_dump += 'user %s;\n' % getpass.getuser()
          elif l.startswith('pid'):
            config_dump += 'pid %s/nginx.pid;\n' % instance_dir
          elif l.lstrip().startswith('access_log'):
            config_dump += '    access_log %s/acces_log.txt;\n' % instance_dir
          else:
            config_dump += l
      with open(instance_dir+'/nginx.conf', 'w+') as f:
        f.write(config_dump)

      # 5. Copy some base files needed for this instance
      shutil.copy(default_dir + '/nginx/mime.types', instance_dir+'/mime.types')
      shutil.copy(default_dir + '/nginx/fastcgi_params', instance_dir+'/fastcgi_params')
      shutil.copy(default_dir + '/nginx/uwsgi_params', instance_dir+'/uwsgi_params')

      self.Close()

    else:
      print "Empty"
