import wx, subprocess as sproc

class ElevateFrame(wx.Frame):

  def __init__(self, parent=None):
    super(ElevateFrame, self).__init__(parent, title="New MySQL instance",
      style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)

    self.panel = wx.Panel(self)
    self.parent = parent

    self.password_text = wx.TextCtrl(
      self.panel, wx.ID_ANY, pos=(80,80), size=(200,30))
    self.ok = wx.Button(self.panel, pos=(80, 150), size=(200,30),label="OK")

    self.ok.Bind(wx.EVT_LEFT_UP, self.on_click)

  def get_password(self):
    self.Show()

  def on_click(self, event):
    # do a test to have a proof of a correct password
    p = sproc.Popen(['sudo','-S','echo', 'a'])
    print event

