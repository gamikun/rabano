import wx


class TaskBar(wx.TaskBarIcon):

  def __init__(self, parent=None, icon=None):
    super(TaskBar, self).__init__()
    self.SetIcon(icon, "Icono")
    self.parent = parent

    # wx.EVT_TASKBAR_RIGHT_UP(self, self.OnMenu)

  def CreatePopupMenu(self):
    menu = wx.Menu()
    restore_menu = menu.Append(wx.ID_ANY, "Open panel")
    menu.AppendSeparator()
    close_menu = menu.Append(wx.ID_ANY, "Close Rabano")
    self.Bind(wx.EVT_MENU, self.CloseRabano, close_menu)
    self.Bind(wx.EVT_MENU, self.RestoreWindow, restore_menu)

    return menu

  def CloseRabano(self, event):
    self.parent.Close()
    wx.GetApp().ExitMainLoop()

  def RestoreWindow(self, event):
    self.parent.Show()



