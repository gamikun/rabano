# -*- coding: utf-8 -*-
from rabano.system import Elevation
import wx, time



class SudoFrame(wx.Frame):

  cached_password = ''
  max_tries = 3

  def __init__(self, command=None, callback=None, info=None, call_this=None):
    super(SudoFrame, self).__init__(None, size=(350, 250))

    self.command = command
    self.callback = callback
    self.info = info
    self.current_try = 0
    self.call_this = call_this

    # Main panel
    self.panel = wx.Panel(self)

    # Password field
    self.text_field = wx.TextCtrl(self.panel, style=wx.TE_PASSWORD,
      size=(200, 25), pos=(50, 50))

    # Accept button
    self.accept_button = wx.Button(self.panel, pos=(50, 100), label="OK")

    # Binding events
    self.accept_button.Bind(wx.EVT_LEFT_UP, self.CheckLogin)

  def Show(self):
    super(SudoFrame, self).Show()
    self.text_field.SetFocus()

  def CheckLogin(self, event):
    if not self.call_this is None:
      retcode = self.call_this(password=self.text_field.GetValue())
    else:
      p = Elevation.popen(command=self.command, with_stdin=True)
      out, err = p.communicate(self.text_field.GetValue() + '\n')
      retcode = p.returncode

    if retcode != 0:
      self.say_no()
    else:
      self.Close()
      if not self.callback is None:
        self.callback(self.info)

  def say_no(self):
    x, y = self.GetPosition()
    low_x, high_x = (x-25, x+25)
    sumer = -10
    for i in range(10):
      x += sumer
      self.SetPosition((x, y))
      if x <= low_x: sumer = 20
      if x >= high_x: sumer = -20
      time.sleep(0.005)
    self.SetPosition((low_x+25, y))
