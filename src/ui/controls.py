import wx

class FlatListItem:
  def __init__(self, title, description, icon=None, status_icon=None,
      info=None):
    self.title = title
    self.description = description
    self.icon = icon
    self.status_icon = status_icon
    self.info = info

class FlatList(wx.PyControl):
  def __init__(self, parent=None, item_height=50, size=(300,150),
               pos=wx.DefaultPosition, default_icon=None,
               default_status_icon=None):
    super(FlatList, self).__init__(parent, size=size, pos=pos)

    self.item_height = item_height
    self.selected_index = None
    self.padding = (5,10,0,10)
    self.default_icon = default_icon
    self.default_status_icon = default_status_icon

    self.Bind(wx.EVT_PAINT, self.OnPaint)
    self.Bind(wx.EVT_ERASE_BACKGROUND, self.OnEraseBackground)
    self.Bind(wx.EVT_LEFT_UP, self.OnMouseUp)
    self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)

    self.items = []
    
  def OnPaint(self, event):
    dc = wx.BufferedPaintDC(self)
    self.Draw(dc)
  def OnEraseBackground(self, event):
    pass

  def OnMouseUp(self, event):
    index = event.y / self.item_height
    if index >= len(self.items):
      self.DoSelection(None)
    else:
      self.DoSelection(index)

  def DoSelection(self, index, do_raise=True):
    self.selected_index = index
    if do_raise == True:
      ev = wx.CommandEvent(wx.EVT_LIST_ITEM_SELECTED._getEvtType(), self.GetId())
      ev.SetEventObject(self)
      self.GetEventHandler().ProcessEvent(ev)
    self.Refresh()

  def OnKeyDown(self, event):
    key = event.GetKeyCode()
    if key == 40:
      if not self.selected_index is None:
        if self.selected_index < len(self.items):
          self.DoSelection(self.selected_index + 1)
    elif key == 38:
      pass

  def Draw(self, dc):
    w, h = self.GetSize()
    pt,pr,pb,pl = self.padding
    dc.SetPen(wx.TRANSPARENT_PEN)
    dc.DrawRectangle(0,0, w, h)

    dc.SetFont(self.Font)

    # Draw every item in the list
    for i, item in enumerate(self.items):
      icon_x = pl
      item_y = i * self.item_height
      text_x = icon_x * 2 + 32
      title_y = item_y + (pt + 2)
      title_width, title_height = dc.GetTextExtent(item.title)
      icon_y = item_y + pt + 3
      description_y = title_y + title_height
      
      if i == self.selected_index:
        dc.SetBrush(wx.Brush('#0066CC', wx.SOLID))
        dc.DrawRectangle(0, item_y, w, self.item_height)
        dc.SetTextForeground('white')
      else:
        dc.SetTextForeground('black')
      # Draw the item icon or the default one
      if not item.icon is None:
        dc.DrawIcon(item.icon, icon_x, icon_y)
      elif not self.default_icon is None:
        dc.DrawIcon(self.default_icon, icon_x, icon_y)

      dc.DrawText(item.title, text_x, title_y)
      if i == self.selected_index:
        dc.SetTextForeground('#ffffff')
      else:
        dc.SetTextForeground('#666666')
      dc.DrawText(item.description, text_x, description_y)

      # Draw the status icon
      if not item.status_icon is None:
        status_icon_y = (self.item_height / 2 - 16 / 2) + (self.item_height*i)
        dc.DrawIcon(item.status_icon, w - 16 - pr, status_icon_y)

  def GetSelected(self):
    if not self.selected_index is None:
      return self.items[self.selected_index]

  def AddItem(self, title, description, icon=None, info=None):
    item = FlatListItem(title, description, icon=icon,
      status_icon=self.default_status_icon, info=info)
    self.items.append(item)
    self.Refresh()

    return item
