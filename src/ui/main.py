# -*- coding: utf8 -*-
from ui.controls import *
from rabano.system import Service, Elevation
from rabano.services import \
  NginxService, PostgresService, MemcachedService, \
  MysqlService, ApacheService
from ui.instances import NginxFrame
from ui.menus import TaskBar
from ui.sudo import SudoFrame
import subprocess as sproc
import time

class MainForm(wx.Frame):
  def __init__(self, parent, size=(640, 375), icons=None, taskbar_icon=None):
    super(MainForm, self).__init__(parent, size=size)
    self.iconos = icons
    self.SetIcon(self.iconos['apache'])
    #self.main_menu = wx.MenuItem()
    self.panel = wx.Panel(self)
    self.list  = FlatList(self.panel, default_icon=self.iconos['apache'],
      size=(300,300), default_status_icon=self.iconos['status-gray'])
    self.button_start = wx.Button(self.panel, wx.ID_ANY, "Start Service", pos=(300,150))
    self.button_start.Disable()
    self.button_stop = wx.Button(self.panel, wx.ID_ANY, "Stop Service", pos=(300,200))
    self.button_stop.Disable()
    self.button_new_profile = wx.Button(self.panel, wx.ID_ANY, "Nuevo perfil",
      pos=(310, 35))
    self.button_new_nginx = wx.Button(self.panel, wx.ID_ANY, "New NGINX",
      pos=(10, 310))
    self.service_title = wx.StaticText(self.panel, wx.ID_ANY, "Esto es genial",
      pos=(310, 5))

    # File Menu
    menu_file = wx.Menu()
    menu_file.Append(wx.ID_EXIT, 'Quit', 'Quit Application')

    # Configuration Menu
    menu_config = wx.Menu()
    menu_edit_config = menu_config.Append(wx.ID_ANY, 'Edit Configuration', 'Edit Configuration')

    # MenuBar
    menu = wx.MenuBar()
    menu.Append(menu_file, '&File')
    menu.Append(menu_config, '&Configuration')

    self.SetMenuBar(menu)

    # Taskbar Icon
    taskbar = TaskBar(icon=taskbar_icon, parent=self)

    # Event handler
    self.button_new_profile.Bind(wx.EVT_LEFT_UP, self.new_profile)
    self.button_start.Bind(wx.EVT_LEFT_UP, self.start_service)
    self.button_stop.Bind(wx.EVT_LEFT_UP, self.stop_service)
    self.button_new_nginx.Bind(wx.EVT_LEFT_UP, self.new_nginx)
    self.Bind(wx.EVT_CLOSE, self.hide_window)
    self.Bind(wx.EVT_TASKBAR_CLICK, self.taskbar_click)

    self.list.Bind(wx.EVT_LIST_ITEM_SELECTED, self.select_instance)
    self.Bind(wx.EVT_KEY_DOWN, self.key_down)
    # wx.EVT_TASKBAR_LEFT_UP(taskbar, self.taskbar_click)

    self.Bind(wx.EVT_MENU, self.menu_click, menu_edit_config)

  def key_down(self, event):
    print "hola"

  def menu_click(self, event):
    print "Yeah"

  def taskbar_click(self, event):
    print "Yeah"

  def completed_command(self, info):
    for s in self.list.items:
      if s.info == info:
        time.sleep(0.15)
        if info.is_running():
          s.status_icon = self.iconos['status-green']
        else:
          s.status_icon = self.iconos['status-gray']
          info.cached_pid = None

        info.is_starting = False
        info.is_stopping = False

        self.list.Refresh()

        if s == self.list.GetSelected():
          self.list.DoSelection(self.list.selected_index, do_raise=True)
        break

  def select_instance(self, event):
    if not self.list.selected_index is None:
      item = self.list.items[self.list.selected_index]
      if item.info.is_running() == True:
        self.button_start.Disable()
        self.button_stop.Enable()
      else:
        self.button_start.Enable()
        self.button_stop.Disable()
    else:
      self.button_start.Disable()
      self.button_stop.Disable()

  def hide_window(self, event):
    if event.CanVeto():
      self.Hide()
      event.Veto()
    else:
      event.Skip()

  def new_profile(self, event):
    newprof = NewProfileFrame(self)
    newprof.Show(True)

  def get_selected_service(self):
    return self.list.items[self.list.selected_index] \
      if not self.list.selected_index is None else None

  def run_something(self, params=None, service=None, with_stdin=False, call_this=None):
    if not call_this is None:
      retcode = call_this()
    else:
      pp = Elevation.popen(command=params, with_stdin=True)
      pp.communicate('.\n')
      retcode = pp.returncode

    if retcode == 1 and service.need_sudo:
      prom = SudoFrame(command=params, info=service,
        callback=self.completed_command, call_this=call_this)
      prom.Show()
    else:
      self.completed_command(service)

  def start_service(self, event):
    """ Starts the selected service """
    item = self.get_selected_service()
    params = None
    call_function = None

    if not item is None:
      service = item.info
      if not service.is_starting \
      and not service.is_stopping \
      and not service.is_running():

        service.is_starting = True

        if isinstance(service, NginxService):
          self.run_something(params=[
            service.binary,
            '-c', service.config_file,
            '-g', 'pid "%s";' % service.pid_file
          ], service=service)

        elif isinstance(service, PostgresService):
          self.run_something(service=service, call_this=service.start)

        elif isinstance(service, MemcachedService):
          self.run_something(service=service, call_this=service.start)

        elif isinstance(service, MemcachedService):
          self.run_something(service=service, call_this=service.start)

        elif isinstance(service, MysqlService):
          self.run_something(service=service, call_this=service.start)

        elif isinstance(service, ApacheService):
          self.run_something(service=service, call_this=service.start)     

      else:
        # Nothing happens
        pass

  def stop_service(self, event):
    """ Stops the selected service """
    item = self.get_selected_service()
    if not item is None:
      service = item.info
      if not service.is_stopping \
      and not service.is_starting \
      and service.is_running():

        service.is_stopping = True

        self.run_something(service=service, call_this=service.stop)

      else:
        # Nothing happens
        pass

  def new_nginx(self, event):
    frame = NginxFrame(self)
    frame.Show()

