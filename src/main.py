# -*- coding: utf-8 -*-
import socket, os, subprocess, data, signal, fcntl, getpass, wx
from ui.instances import NginxFrame
from functions import get_instance_dir
from multiprocessing import Process
from threading import Thread
from ui.main import MainForm
from rabano.system import Service
from rabano.services import NginxService, \
  PostgresService, MemcachedService, \
  MysqlService, ApacheService

class DaemonTemplate:
  def __init__(self, template, params=None, category="other"):
    self.template = template
    self.params = params 
    self.category = category

"""
class NewProfileFrame(wx.Frame):
  def __init__(self, parent):
    super(NewProfileFrame, self).__init__(parent)
    self.panel = wx.Panel(self)
    # Labels and textboxes
    self.label_bin = wx.StaticText(self.panel, wx.ID_ANY,
      "Binario:", pos=(35, 35))
    self.text_bin = wx.TextCtrl(self.panel, wx.ID_ANY,
      pos=(100,35), size=(200, 25))
"""

if __name__ == '__main__':
  app = wx.App()

  ICONOS = {
    'apache': wx.Icon('images/apache.png', wx.BITMAP_TYPE_PNG),
    'status-green' : wx.Icon('images/status-green.png', wx.BITMAP_TYPE_PNG),
    'status-gray' : wx.Icon('images/status-gray.png', wx.BITMAP_TYPE_PNG),
    'taskbar-icon-mac' : wx.Icon('images/tb-icon.png', wx.BITMAP_TYPE_PNG),
  }

  top = MainForm(None, icons=ICONOS, taskbar_icon=ICONOS['taskbar-icon-mac'])

  # Loads every instance and put it in main list
  instances = data.get_all_instances()
  for inst in instances:
    id,name,binary,params,pid_file,type,logfile,config_file,data_dir = inst

    # Servidor Web NGINX

    if type == 'nginx':
      service = NginxService(binary,
        config_file=config_file,
        pid_file=pid_file
      )

      item = top.list.AddItem(name, u"NGINX",
        icon=wx.Icon("images/nginx.png", wx.BITMAP_TYPE_PNG),
        info=service
      )

      if service.is_running() == True:
        item.status_icon = ICONOS['status-green']

    # PostgreSQL (database)
    if type == 'postgresql':
      service = PostgresService(binary,
        data_dir=data_dir
      )

      item = top.list.AddItem(name, u"PostgreSQL",
        icon=wx.Icon("images/postgre-sql.png"),
        info=service
      )

      if service.is_running() == True:
        item.status_icon = ICONOS['status-green']

    # PHP-FPM
    if type == 'php-fpm':
      service = PhpService(binary,
        config_file=config_file,
        pid_file=pid_file,
      )

      item = top.list.AddItem(name, u"PostgreSQL",
        icon=wx.Icon("images/postgre-sql.png"),
        info=service
      )

      if service.is_running() == True:
        item.status_icon = ICONOS['status-green']

    if type == 'memcached':
      service = MemcachedService(binary,
        pid_file=pid_file
      )

      item = top.list.AddItem(name, u"Memcached",
        icon=wx.Icon("images/postgre-sql.png"),
        info=service
      )

      if service.is_running() == True:
        item.status_icon = ICONOS['status-green']

    if type == 'mysql':
      service = MysqlService(binary,
        pid_file=pid_file
      )

      item = top.list.AddItem(name, u"MySQL",
        icon=wx.Icon("images/postgre-sql.png"),
        info=service
      )

      if service.is_running():
        item.status_icon = ICONOS['status-green']

    if type == 'apache':
      service = ApacheService(binary,
        pid_file=pid_file
      )

      item = top.list.AddItem(name, u"Apache",
        icon=wx.Icon("images/apache.png"),
        info=service
      )

      if service.is_running():
        item.status_icon = ICONOS['status-green']

  top.Show()
  app.MainLoop()
