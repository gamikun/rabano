import os, data, subprocess as sproc, re
from rabano.system import find_process_output

__config_dir = None

def get_config_dir():
  """ Returns configuration directory """
  return os.path.expanduser('~/.rabano')

def get_default_config_dir(type=None):
  """ Default dir is where default configuration
      for future instances creation is stored. """
  return get_config_dir() + '/default' + \
    ('/'+type if not type is None else '')

def get_instance_dir(instance_id):
  """ Given a instance id, it Returns
      the respective folder in config folder """
  return get_config_dir() + '/instances/' + str(instance_id)

def create_instance(name, binary='', type=None):
  """ Insert a instance in database and 
      make the neccessary folders in config dir """
  instance_id = data.save_instance(name, binary, type=type)
  assert not instance_id is None
  assert os.path.isfile(binary)

  config_dir = get_config_dir()
  if os.path.isdir(config_dir) == False:
    os.mkdir(config_dir)

  instance_dir = get_instance_dir(instance_id)
  os.mkdir(instance_dir)

  return instance_id

